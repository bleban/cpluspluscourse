class Polygon {
public:
    Polygon(int n, float radius);
    virtual ~Polygon(){};
    virtual float computePerimeter(); // virtual causes to apply computePerimeter od descendants
protected:
    int m_nbSides;
    int m_radius;
};

class Pentagon : public Polygon {
public:
    Pentagon(float radius);
};

class Hexagon : public Polygon {
public:
    Hexagon(float radius);
    // 6*radius is easier than generic case
    float computePerimeter();
};
