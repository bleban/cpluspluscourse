#include "Polygons.hpp"
#include <iostream>

int main() {
    // create a Pentagon, call its perimeter method
    Pentagon MyPent(10.);
    std::cout << "\n1. Pentagon perimeter: " << MyPent.computePerimeter() << std::endl;

    // create an Hexagon, call its perimeter method
    Hexagon MyHex(10.);
    std::cout << "\n2. Hexagon perimeter: " << MyHex.computePerimeter() << std::endl;

    // create an Hexagon, call its parent's perimeter method
    Hexagon MyHex1(10.);
    Polygon * polyHex = &MyHex1;

    std::cout << "\n3. Hexagon's parent perimeter: " << polyHex->computePerimeter() << std::endl;


    // retry with virtual methods

}
