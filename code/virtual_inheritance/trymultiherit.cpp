#include "TextBox.hpp"
#include <iostream>

int main() {
    // create a TextBox and call draw
    TextBox MyTextBox("Virtual inheritance", 300, 300);

    // Fix the code to call both draws by using types
    Rectangle * p_rect = &MyTextBox;
    p_rect->draw();

    Text * p_text = &MyTextBox;
    p_text->draw();

    // try with virtual inheritance


}
