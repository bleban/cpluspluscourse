#include <iostream>
#include <math.h>
#include <algorithm>
#include <vector>
#include <ext/numeric>
#include "Complex.hpp"

using namespace std;
using namespace __gnu_cxx;

template<class T>
void compute(int len, T initial, T step) {
    // allocate vectors
    std::vector<T> v(len+1), diffs(len+1);

    // fill and randomize v
    T cur_value = initial;
    generate(v.begin(), v.end(), [&cur_value, step](){ return cur_value += step; });
    random_shuffle(v.begin(), v.end());
    
    // compute differences
    adjacent_difference(v.begin(), v.end(), diffs.begin());

    // compute standard deviation of it
    T sum = accumulate(diffs.begin(), diffs.end(), T());
    T sumsq = accumulate(diffs.begin(), diffs.end(), T(), [](T& a, T& b) { return a + b * b; });
    T mean = sum/len;
    T variance = sumsq/len - mean*mean;

    std::cout << "Range = [" << initial << ", " << step*len << "]\n"
              << "Mean = " << mean << "\n"
              << "Variance = " << variance << std::endl;
}

int main() {
    compute(1000, 0.0, 7.0);
}
