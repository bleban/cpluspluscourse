import numpy as np
import matplotlib.pyplot as plt
from mandel import mandel


X = np.arange(-2, .5, .002)
Y = np.arange(-1,  1, .002)
Z = np.zeros((len(Y), len(X)))

for iy, y in enumerate(Y):
    print(iy, "of", len(Y))
    for ix, x in enumerate(X):
        a = mandel(x, y)
        if a >= 0:
            Z[iy, ix] = a
        else:
            Z[iy, ix] = np.nan


plt.imshow(Z, cmap=plt.cm.prism, interpolation='none', extent=(X.min(), X.max(), Y.min(), Y.max()))
plt.xlabel("Re(c)")
plt.ylabel("Im(c)")
plt.savefig("mandelbrot_python.png")
# plt.show()
